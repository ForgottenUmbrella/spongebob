use std::io::{self, Read, Write};
use rand::Rng;

// Probability of not toggling case.
const ERROR_RATE: f64 = 0.1;

/// Prints stdin input in mOcKinG cAsE.
fn main() -> io::Result<()> {
    let stdout = io::stdout();
    // Buffer the stdout for performance or something.
    let mut stdout_writer = io::BufWriter::new(stdout.lock());
    let mut rng = rand::thread_rng();
    let mut is_lowercase = true;
    // The stdin is buffered too through the lock.
    for byte in io::stdin().lock().bytes() {
        let case = if is_lowercase { u8::to_ascii_lowercase } else { u8::to_ascii_uppercase };
        let c = case(&byte?);
        stdout_writer.write_all(&[c])?;
        // Only toggle case if we had a letter and don't make an "error".
        is_lowercase ^= c.is_ascii_alphabetic() && rng.gen::<f64>() > ERROR_RATE;
    }
    stdout_writer.flush()
}
